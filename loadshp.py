import shapefile

#file = shapefile.Reader("County_Address_Points_IDHS_IN_Allen/County_Address_Points_IDHS_IN_Dec2015_Allen")
#file = shapefile.Reader("County_Address_Points_IDHS_IN_Allen/splits/sub-splits/46835/1")
file = shapefile.Reader("County_Address_Points_IDHS_IN_Allen/splits/County_Address_Points_IDHS_IN_Dec2015_Allen_ZIPCODE_46835")

shapeRecs = file.shapeRecords()
print("Loaded Shape file")

dupesfile = shapefile.Writer(shapefile.POINT)
dupesfile.field('ADDR_HN') #      [0]
dupesfile.field('ADDR_PD') #      [1]
dupesfile.field('ADDR_PT') #      [2]
dupesfile.field('ADDR_SN') #      [3]
dupesfile.field('ADDR_ST') #      [4]
dupesfile.field('ADDR_SD') #      [5]
dupesfile.field('FID_')    #      [6]
dupesfile.field('SOURCEID') #     [7]
dupesfile.field('ORIGIN') #       [8]
dupesfile.field('LOADDATE') #     [9]
dupesfile.field('ADDRESS') #      [10]
dupesfile.field('NUMBER') #       [11]
dupesfile.field('SUFFIX') #       [12]
dupesfile.field('ADD_TYPE') #     [13]
dupesfile.field('PREFIX') #       [14]
dupesfile.field('ADD_NAME') #     [15]
dupesfile.field('ADD_CITY') #     [16]
dupesfile.field('COUNTY') #       [17]
dupesfile.field('STATE') #        [18]
dupesfile.field('CURRENT_ST') #   [19]
dupesfile.field('ZIPCODE') #      [20]
dupesfile.field('CNTY_ID') #      [21]
dupesfile.field('FGEO_ADD') #     [22]
dupesfile.field('UNITTYPE') #     [23]
dupesfile.field('UNITID') #       [24]

print("Setup dupesfile")

keepfile = shapefile.Writer(shapefile.POINT)
keepfile.field('ADDR_HN') #      [0]
keepfile.field('ADDR_PD') #      [1]
keepfile.field('ADDR_PT') #      [2]
keepfile.field('ADDR_SN') #      [3]
keepfile.field('ADDR_ST') #      [4]
keepfile.field('ADDR_SD') #      [5]
keepfile.field('FID_')    #      [6]
keepfile.field('SOURCEID') #     [7]
keepfile.field('ORIGIN') #       [8]
keepfile.field('LOADDATE') #     [9]
keepfile.field('ADDRESS') #      [10]
keepfile.field('NUMBER') #       [11]
keepfile.field('SUFFIX') #       [12]
keepfile.field('ADD_TYPE') #     [13]
keepfile.field('PREFIX') #       [14]
keepfile.field('ADD_NAME') #     [15]
keepfile.field('ADD_CITY') #     [16]
keepfile.field('COUNTY') #       [17]
keepfile.field('STATE') #        [18]
keepfile.field('CURRENT_ST') #   [19]
keepfile.field('ZIPCODE') #      [20]
keepfile.field('CNTY_ID') #      [21]
keepfile.field('FGEO_ADD') #     [22]
keepfile.field('UNITTYPE') #     [23]
keepfile.field('UNITID') #       [24]

print("Setup keepfile")

i = 0
while i < len(shapeRecs):
    address = shapeRecs[i]
    r = address.record

    ii = 0
    dupe_exists = False
    while ii < len(shapeRecs):
        address2 = shapeRecs[ii]
        if address.shape.points[0][0] == address2.shape.points[0][0] and address.shape.points[0][1] == address2.shape.points[0][1] and not address.record[7] == address2.record[7]:
            dupe_exists = True
            #print(address.record[0], " ", address.record[3], " ", address.record[4] , "\tzip: ", address.record[20], "\tLat: ", address.shape.points[0][0], "\tLon: ", address.shape.points[0][1], "\tID:",address.record[6])
            #dupesfile.point(address.shape.points[0][0],address.shape.points[0][1])
            #dupesfile.record(r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11],r[12],r[13],r[14],r[15],r[16],r[17],r[18],r[19],r[20],r[21],r[22],r[23],r[24])
        #else:
            #print(address.record[0], " ", address.record[3], " ", address.record[4] , "\tzip: ", address.record[20], "\tLat: ", address.shape.points[0][0], "\tLon: ", address.shape.points[0][1], "\tID:",address.record[6])
            #keepfile.point(address.shape.points[0][0],address.shape.points[0][1])
            #keepfile.record(r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11],r[12],r[13],r[14],r[15],r[16],r[17],r[18],r[19],r[20],r[21],r[22],r[23],r[24])
        ii +=1
    
    if dupe_exists:
        print("DUPE:", address.record[0], " ", address.record[3], " ", address.record[4] , "\tzip: ", address.record[20], "\tLat: ", address.shape.points[0][0], "\tLon: ", address.shape.points[0][1], "\tID:",address.record[6])
        dupesfile.point(address.shape.points[0][0],address.shape.points[0][1])
        dupesfile.record(r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11],r[12],r[13],r[14],r[15],r[16],r[17],r[18],r[19],r[20],r[21],r[22],r[23],r[24])
    else:
        print("CLEAN:", address.record[0], " ", address.record[3], " ", address.record[4] , "\tzip: ", address.record[20], "\tLat: ", address.shape.points[0][0], "\tLon: ", address.shape.points[0][1], "\tID:",address.record[6])
        keepfile.point(address.shape.points[0][0],address.shape.points[0][1])
        keepfile.record(r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11],r[12],r[13],r[14],r[15],r[16],r[17],r[18],r[19],r[20],r[21],r[22],r[23],r[24])
		
    i += 1
    
keepfile.save('output/46835/no_dupes')
dupesfile.save('output/46835/dupes')
