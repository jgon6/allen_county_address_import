import shapefile
import json

nodupes_file = shapefile.Reader('output/46835/no_dupes')
nodupesRecs = nodupes_file.shapeRecords()

jsondata = json.loads(open ('export.geojson').read())

def isinosm(address):
    j = 0
    #print(address[10])
    while j < len(jsondata["features"]):
        feature = jsondata["features"][j]["properties"]
        
        # if the osm entry has a street name.
        if "addr:street" in feature.keys():
            
            
            # here we convert shortened street types into the full types 
            streettype = address[4]
            if address[4] == "Ave":
                streettype = "Avenue"
            if address[4] == "St Extension":
                streettype = "Street Extension"
            if address[4] == "Ave Extension":
                streettype = "Avenue Extension"
            if address[4] == "St":
                streettype = "Street"
            if address[4] == "Hwy":
                streettype = "Highway"
                
            street = (address[1] + " " + address[2] + " " +address[3] + " " + address[4] + " " + address[5]).strip()
            #print((address[3] + " " + address[4]).lower())
            if address[0] == feature["addr:housenumber"] and (street).lower() == feature["addr:street"].lower():
                return True
        
        j += 1
    return False

existsfile = shapefile.Writer(shapefile.POINT)
existsfile.field('ADDR_HN') #      [0]
existsfile.field('ADDR_PD') #      [1]
existsfile.field('ADDR_PT') #      [2]
existsfile.field('ADDR_SN') #      [3]
existsfile.field('ADDR_ST') #      [4]
existsfile.field('ADDR_SD') #      [5]
existsfile.field('FID_')    #      [6]
existsfile.field('SOURCEID') #     [7]
existsfile.field('ORIGIN') #       [8]
existsfile.field('LOADDATE') #     [9]
existsfile.field('ADDRESS') #      [10]
existsfile.field('NUMBER') #       [11]
existsfile.field('SUFFIX') #       [12]
existsfile.field('ADD_TYPE') #     [13]
existsfile.field('PREFIX') #       [14]
existsfile.field('ADD_NAME') #     [15]
existsfile.field('ADD_CITY') #     [16]
existsfile.field('COUNTY') #       [17]
existsfile.field('STATE') #        [18]
existsfile.field('CURRENT_ST') #   [19]
existsfile.field('ZIPCODE') #      [20]
existsfile.field('CNTY_ID') #      [21]
existsfile.field('FGEO_ADD') #     [22]
existsfile.field('UNITTYPE') #     [23]
existsfile.field('UNITID') #       [24]

print("Setup existsfile")

newfile = shapefile.Writer(shapefile.POINT)
newfile.field('ADDR_HN') #      [0]
newfile.field('ADDR_PD') #      [1]
newfile.field('ADDR_PT') #      [2]
newfile.field('ADDR_SN') #      [3]
newfile.field('ADDR_ST') #      [4]
newfile.field('ADDR_SD') #      [5]
newfile.field('FID_')    #      [6]
newfile.field('SOURCEID') #     [7]
newfile.field('ORIGIN') #       [8]
newfile.field('LOADDATE') #     [9]
newfile.field('ADDRESS') #      [10]
newfile.field('NUMBER') #       [11]
newfile.field('SUFFIX') #       [12]
newfile.field('ADD_TYPE') #     [13]
newfile.field('PREFIX') #       [14]
newfile.field('ADD_NAME') #     [15]
newfile.field('ADD_CITY') #     [16]
newfile.field('COUNTY') #       [17]
newfile.field('STATE') #        [18]
newfile.field('CURRENT_ST') #   [19]
newfile.field('ZIPCODE') #      [20]
newfile.field('CNTY_ID') #      [21]
newfile.field('FGEO_ADD') #     [22]
newfile.field('UNITTYPE') #     [23]
newfile.field('UNITID') #       [24]

print("Setup newfile")

i = 0
while i < len(nodupesRecs):
    address = nodupesRecs[i]
    r = address.record
    
    status = isinosm(r)
    if status:
        label = "[EXISTS]"
        existsfile.point(address.shape.points[0][0],address.shape.points[0][1])
        existsfile.record(r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11],r[12],r[13],r[14],r[15],r[16],r[17],r[18],r[19],r[20],r[21],r[22],r[23],r[24])
    else:
        label = "[IS NEW]"
        newfile.point(address.shape.points[0][0],address.shape.points[0][1])
        newfile.record(r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11],r[12],r[13],r[14],r[15],r[16],r[17],r[18],r[19],r[20],r[21],r[22],r[23],r[24])
        
    print(label,r[10])
    
    i += 1

existsfile.save('step3/46835/exists')
newfile.save('step3/46835/new')
