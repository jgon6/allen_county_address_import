import shapefile

#file = shapefile.Reader('step3/46835/new')
file = shapefile.Reader("County_Address_Points_IDHS_IN_Allen/County_Address_Points_IDHS_IN_Dec2015_Allen")
fileRecs = file.shapeRecords()


osmfile = shapefile.Writer(shapefile.POINT)
osmfile.field('number')
osmfile.field('street')
osmfile.field('city')
osmfile.field('postcode')

i = 0
while i < len(fileRecs):
    address = fileRecs[i]
    r = address.record
    
    # here we convert shortened street types into the full types 
    streettype = r[4]
    if r[4] == "Ave":
        streettype = "Avenue"
    if r[4] == "St Extension":
        streettype = "Street Extension"
    if r[4] == "Ave Extension":
        streettype = "Avenue Extension"
    if r[4] == "St":
        streettype = "Street"
    if r[4] == "Hwy":
        streettype = "Highway"
    street = (r[1] + " " + r[2] + " " +r[3] + " " + r[4] + " " + r[5]).strip()
    
    # Most of the citys are in the dataset as two letter codes, here we will expand them
    city = r[16]
    if r[16] == "AU":
        city = "Auburn"
    if r[16] == "CH":
        city = "Churubusco"
    if r[16] == "DE":
        city = "Decatur"
    if r[16] == "FW":
        city = "Fort Wayne"
    if r[16] == "GR":
        city = "Grabill"
    if r[16] == "HA":
        city = "Harlan"
    if r[16] == "HO":
        city = "Hoagland"
    if r[16] == "HU":
        city = "Huntertown"
    if r[16] == "LC":
        city = "Leo-Cedarville"
    if r[16] == "MO":
        city = "Monroeville"
    if r[16] == "NH":
        city = "New Haven"
    if r[16] == "OS":
        city = "Ossian"
    if r[16] == "RO":
        city = "Roanoke"
    if r[16] == "SP":
        city = "Spencerville"
    if r[16] == "WO":
        city = "Woodburn"
    if r[16] == "YO":
        city = "Yoder"
    
    osmfile.point(address.shape.points[0][0],address.shape.points[0][1])
    osmfile.record(r[0],street.title(),city.title(),r[20])
    print(r[0])
    
    i += 1
    
osmfile.save('step4/46835/osm-test')
