# Allen county addesses import scripts 

Scripts for the import of addresses from allen county into OpenStreetMap.

| Script name | Description |
| ----------- | ----------- |
| loadshp.py  | Loads the shape file and removes the addresses points with duplicate coords |
| pre-existing.py | Removes the points that are already in osm |
| final-convert.py | Formats the address data into the right format\* |

\* final-convert.py can't use the right key values due to limitation of the shapefile format.

### Import Process

```mermaid

graph TD


A[County_Address_Points_IDHS_IN_Dec2015_Allen.shp]-->B{Qgis}
B-->C1[splits/County_Address_Points_IDHS_IN_Dec2015_Allen_ZIPCODE_43526.shp]
B-->C2[splits/County_Address_Points_IDHS_IN_Dec2015_Allen_ZIPCODE_43513.shp]
B-->C3[splits/County_Address_Points_IDHS_IN_Dec2015_Allen_ZIPCODE_nnnnn.shp]
C1-->D{loadshp.py}
D--> |Points share coordinates| E[output/<zip>/dupes]
D--> |Clean Points| F[output/<zip>/no-dupes]
G[export.geojson]-->H[pre-existing.py]
F-->H
H-->I[new.shp]
H-->J[exists.shp]
I-->K{final-convert.py}
K-->L[osm.shp]
L-->M{JOSM}
M-->N[osm.osm]
N-->O{Text Editor}
O-->P[osm.osm V2]

```
